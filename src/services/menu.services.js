module.exports = {

    // Permite obtener los datos de DB o servicio
    getMenu: (callback) => {
        setTimeout(() => {

            // Simular el tiempo de traer la informacion de la DB o servicio
            callback(null, [
                {
                    "label": "Home",
                    "icon": "fa-plus",
                    "url": "/",
                    "children": [
                    {
                        "label": "Page 1",
                        "icon": "fa-plus",
                        "url": "page 1"
                    }
                    ]
                },
                {
                    "label": "Tools",
                    "icon": "fa-plus",
                    "url": "/",
                    "children": [
                    {
                        "label": "Page 2",
                        "icon": "fa-plus",
                        "url": "page 2"
                    },
                    {
                        "label": "Page 3",
                        "icon": "fa-plus",
                        "url": "page 3"
                    },
                    {
                        "label": "Page 4",
                        "icon": "fa-plus",
                        "url": "page 4"
                    }
                    ]
                }

            ])

        }, 1000)
    }

}