var menuService = require('../services/menu.services')

module.exports =  {
    // Permite obtener los datos del menu
    getMenu: (callback) =>  {
        // Consume el servicio  con los datos del menu
        menuService.getMenu((error, data) => {
            if (error) {
                callback(true,[])
            }
            callback(null,data)
        })
    }
}